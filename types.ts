export type UserFormData = {
    firstName: string;
    lastName: string;
    age: number,
    salary: string
    position: Position;
    isEmployeed: boolean;
}

export type UserApplicationRequest = {
    fullname: string;
    age: number,
    salary: string
    position: number;
    isEmployeed: boolean;
}

export type Position = 'Developer' | 'Product' | 'Devman'

export const PositionDic: Record<Position, number> = {
    'Developer': 1,
    'Product': 2,
    'Devman': 3
}