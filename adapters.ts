import { PositionDic, UserApplicationRequest, UserFormData } from "./types";

export function processData(userData: UserFormData): UserApplicationRequest {
    return {
        fullname: `${userData.firstName} ${userData.lastName}`,
        age: +userData.age,
        isEmployeed: userData.isEmployeed,
        position: PositionDic[userData.position],
        salary: userData.salary,
    }
}