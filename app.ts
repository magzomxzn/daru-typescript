import { processData } from "./adapters";
import { UserFormData } from "./types";


function init() {
    const form = document.getElementById('userForm');
  
   form?.addEventListener('submit', handleSubmit)
}

function handleSubmit(e: SubmitEvent) {
    e.preventDefault();
    const form = e.target as HTMLFormElement;
    if (!form) {
        return;
    }
    const data = getFormData(form);
    if (data) {
       console.log(processData(data));
    }
}

function getFormData(form: HTMLFormElement): UserFormData | undefined {
    if (!form) {
        return;
    }
    const firstName: string = form['firstName'].value as string;
    const lastName: string = form['lastName'].value;
    const age = form['age'].value as string;
    const salary = form['salary'].value as string;

    const userData: UserFormData = {
        firstName,
        lastName,
        age: +age,
        salary,
        position: 'Developer',
        isEmployeed: false
    }

    return userData;
}


init();